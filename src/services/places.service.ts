import { Storage } from '@ionic/storage';
import {Injectable} from "@angular/core";
import { Geolocation } from '@ionic-native/geolocation';
import {Place} from "../model/place.model";

@Injectable()
export class PlacesService {
  private places: Place[] = [];

  constructor(private storage: Storage) {

  }

  addPlace(place: Place) {
    this.places.push(place);
    this.storage.set('places', this.places);
  }

  deletePlace(place: Place) {
    return new Promise( (resolve, reject) =>
    {
      if(this.places.indexOf(place) != -1) {
        console.log('deleteing', this.places.indexOf(place));
        this.places.splice(this.places.indexOf(place),1);
        this.storage.set('places', this.places);
        resolve(true);
      }
    })

  }

  getPlaces() {
    return this.storage.get('places').then(
      (places) => {
        console.log("plajes",places);
        this.places = (places == null) ? [] : places;
        return this.places.slice();
      });
  }
}
