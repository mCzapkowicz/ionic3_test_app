import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {PlacesService} from "../../services/places.service";
import {Geolocation} from "@ionic-native/geolocation";


@Component({
  selector: 'page-new-place',
  templateUrl: 'new-place.html',
})
export class NewPlace {

  private location: {lat: number, lng: number} = {lat: 0,lng: 0};

  constructor(private placesService: PlacesService, private navController: NavController, private geolocation: Geolocation) {
  }

  onPlaceAdd(value: {title: string}) {
      console.log("adding", value);
      this.placesService.addPlace({title: value.title, location: this.location});
      this.navController.pop();
  }

  onLocateUser() {
    this.geolocation.getCurrentPosition().then(
      (location) => {
        this.location.lng = location.coords.longitude;
        this.location.lat = location.coords.latitude;
    }).catch( (err) => {
      console.log(err)
    });
  }
}
