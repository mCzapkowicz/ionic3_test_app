import { Component } from '@angular/core';
import {NavController, ModalController} from 'ionic-angular';
import {NewPlace} from "../new-place/new-place";
import {PlacesService} from "../../services/places.service";
import {PlacePage} from "../place/place";
import {Place} from "../../model/place.model";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  places: {title: string}[] = [];
  constructor(public navCtrl: NavController, private placesService: PlacesService, private modalCtr: ModalController) {

  }

  ionViewWillEnter() {
    this.getPlaces();
  }

  onDeletePlace(place: Place) {
    this.placesService.deletePlace(place).then((resolve) =>{
      this.getPlaces();
    });
  }

  onLoadNewPlace() {
    this.navCtrl.push(NewPlace);
  }

  onPlaceOpen(place: Place) {
    this.modalCtr.create(PlacePage,place).present();
  }

  getPlaces() {
    this.placesService.getPlaces().then(
      (places)=> {
        console.log('new places', places)
        this.places = places;
      }
    );
  }
}
