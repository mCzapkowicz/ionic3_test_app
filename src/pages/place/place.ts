import { Component } from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';

@Component({
  selector: 'page-place',
  templateUrl: 'place.html',
})
export class PlacePage {

  lat: number;
  lng: number;

  constructor(private viewCtr: ViewController, private navParams: NavParams) {
    this.lng = this.navParams.data.location.lng;
    this.lat = this.navParams.data.location.lat;
  }

  onDismiss() {
    this.viewCtr.dismiss();
  }
}
